#include <iostream>
#include <conio.h>
#include <fstream>




void createMadLib(std::string userInput[10], std::ostream &os)
{	
	    os <<  "The " 
			<< userInput[0] << " Dragon is the " 
			<< userInput[1] << " Dragon of all. It has " 
			<< userInput[2] << " " 
			<< userInput[3] << ", and a " 
			<< userInput[4] << " shaped like a " 
			<< userInput[5] << ". It loves to eat " 
			<< userInput[6] << ", although it will feast on nearly anything. It is " 
			<< userInput[7] << " and " 
			<< userInput[8] << ". You must be " 
			<< userInput[9] << " around it, or you may end up as it`s meal!\n";
}



void saveFile(std::string userInput[10])
{
	std::string input;
	std::cout << "Do you want to save this file? (Y/N) \n";
	std::cin >> input;
	if(input == "y" || input == "Y")
	{
		//"SoftwareDevTests" can be changed to whatever path works on your machine
		std::string path = "C:\\SoftwareDevTests\\test.txt";

		//Create an instance of the ofstream object (long name "madLibFileStreamObject" to help me learn and not just mindlessly copy/paste)
		std::ofstream madLibFileStreamObject(path);
		//Call showMadLib function again
		createMadLib(userInput, madLibFileStreamObject);
		madLibFileStreamObject.close();
		
		//Give confirmation
		std::cout << "File saved successfully";

	}
	else 
	{
		std::cout << "Goodbye";
		//If we wanted to always exit, place exit(0) below the else closing bracket
		exit(0);
	}
}



int main()
{
	std::string userInput[10];
	std::string prompt[10] = {
		"Please enter a color \n",
		"Please enter a superlative (ending in \"est\") \n",
		"Please enter an adjective \n",
		"Please enter a body part (plural) \n",
		"Please enter a body part (single) \n",
		"Please enter a noun \n" ,
		"Please enter an animal (plural) \n",
		"Please enter an adjective \n",
		"Please enter an adjective \n",
		"Please enter an adjective \n"
	};

	for (int i =0; i < 10; i++ ) 
	{
		std::cout << prompt[i];
		std::cin >> userInput[i];
	}

	//Call showMadLib function
	createMadLib(userInput, std::cout);

	//Ask to save
	saveFile(userInput);


	_getch();
	return 0;
}



